﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Note note1, note2, note3;
            note1 = new Note(); //koristenje defaultnog konstruktora tj kreacija objekta
            note2 = new Note("Nad Andreja", "Ovo je prva zabiljeska", 1); //parametarski konstruktor
            note3 = new Note(ref note2); //konstruktor koji kreira biljesku preko reference druge biljeske

            note1.Notes = "Ovo je treca biljeska";
            note1.Priority = 2;

            Console.WriteLine(note1.ToString());
            Console.WriteLine(note2.ToString());
            Console.WriteLine(note3.ToString());


            Console.ReadKey();
        }
    }
}
