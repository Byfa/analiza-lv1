﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            ToDoList notes = new ToDoList();

            string author;
            string note;
            int priority;

            author= Console.ReadLine();
            note = Console.ReadLine();
            Int32.TryParse(Console.ReadLine(), out priority);
            notes.Add(author, note, priority);

            author = Console.ReadLine();
            note = Console.ReadLine();
            Int32.TryParse(Console.ReadLine(), out priority);
            notes.Add(author, note, priority);

            author = Console.ReadLine();
            note = Console.ReadLine();
            Int32.TryParse(Console.ReadLine(), out priority);
            notes.Add(author, note, priority);

            //notes.Add("Nad","first note with highest priority 0",0);
            //notes.Add("Nad", "second note with priority 1",1);
            //notes.Add("Nad", "third note with priority 2",2);


            Console.WriteLine(notes.ToString());
            notes.RemoveAllHighestPriorityNotes();
            Console.WriteLine(notes.ToString());

            Console.ReadKey();
        }
    }
}
