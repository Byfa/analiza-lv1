﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class ToDoList
    {
        private List<TimeNote> notes { get; set; }

        public ToDoList()
        {
            notes = new List<TimeNote>();
        }

        public int Add(string author, string text, int priority, int year, int month, int day)
        {
            TimeNote note = new TimeNote(author, text, priority, year, month, day);
            notes.Add(note);
            return notes.IndexOf(note);
        }

        public int Add(string author, string text, int priority)
        {
            TimeNote note = new TimeNote(author, text, priority);
            notes.Add(note);
            return notes.IndexOf(note);
        }

        public int Add(ref TimeNote note)
        {
            notes.Add(note);
            return notes.IndexOf(note);
        }

        public TimeNote Get(int index)
        {
            return notes[index];
        }

        public void Remove(int index)
        {
            notes.RemoveAt(index);
        }

        public override string ToString()
        {
            StringBuilder note_in_string = new StringBuilder();
            foreach (TimeNote note in notes)
            {
                note_in_string.Append(note.ToString()).Append(Environment.NewLine);
            }
            return note_in_string.ToString();
        }

        private int GetHighestPriorityNumber()
        {
            int priority = 0;

            if (notes.Count > 0)
            {
                priority = notes[0].Priority;
            }
            foreach (TimeNote note in notes)
            {
                if (priority > note.Priority)
                {
                    priority = note.Priority;
                }
            }
            return priority;
        }

        public void RemoveAllHighestPriorityNotes()
        {
            int priority = GetHighestPriorityNumber();

            for (int i = 0; i < notes.Count; i++)
            {
                if (priority == notes[i].Priority)
                {
                    notes.RemoveAt(i);
                    i--;
                }
            }
        }
    }
}
