﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class TimeNote : Note
    {
        private DateTime time { get; set; }

        public void setTime(int year, int month, int day)
        {
            time = new DateTime(year, month, day);
        }

        public void setTime()
        {
            time = DateTime.Now;
        }

        public TimeNote() : base()
        {
            time = DateTime.Now;
        }

        public TimeNote(string author, string note, int priority) : base(author, note, priority)
        {
            time = DateTime.Now;
        }

        public TimeNote(string author, string note, int priority, int year, int month, int day) : base(author, note, priority)
        {
            time = new DateTime(year, month, day);
        }

        public TimeNote(ref TimeNote original) : base(original.Author, original.Notes, original.Priority)
        {
            this.time = original.time; 
            //time = new DateTime(original.time.ToFileTime());
        }

        public override string ToString()
        {
            return base.ToString() + " ; " + time.ToString();
        }
    }
}
