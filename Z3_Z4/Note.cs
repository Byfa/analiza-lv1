﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Note
    {
        private string author { get; } 
        private string note { get; set; }
        private int priority { get; set; }

        public string Author
        {
            get { return this.author; }
            private set {this.author = value; }
        }
        public string Notes {
            get { return this.note; }
            set { this.note = value; }
        }
        public int Priority {
            get { return this.priority; }
            set { this.priority = value; }
        }

        public Note()
        {
            author = "Default";
            note = "Default";
            priority = 5; //5 je min, 0 max
        }

        public Note(string author, string note, int priority)
        {
            this.author = author;
            this.note = note;

            if (priority > 5 || priority < 0) this.priority = 5;
            else
                this.priority = priority; //5 je min, 0 max
        }

        public Note(ref Note original)
        {
            this.author = original.author;
            this.note = original.note;
            this.priority = original.priority;}
        }
}
